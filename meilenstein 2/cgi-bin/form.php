 <!DOCTYPE html>
<html>
<body>
<h1>Evaluated form data</h1>
<table>
<tr>
    <th>Field</th>
    <th>Value</th>
</tr>
<tr><td>Text</td><td><?php echo $_POST['text'] ?></td></tr>
<tr><td>Password-SHA256</td><td><?php echo hash("sha256", $_POST['password']) ?></td></tr>
<tr><td>Email</td><td><?php echo $_POST['email'] ?></td></tr>
<tr><td>Date</td><td><?php echo $_POST['date'] ?></td></tr>
<tr><td>Option</td><td><?php echo $_POST['option'] ?></td></tr>
</table>

</body>
</html> 
