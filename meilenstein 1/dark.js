var dark = {
  storage: null,
  toggle: function(){
      var isDark = dark.storage.getItem('dark') === "true";
      document.body.classList.toggle('dark');
      if(isDark){
        dark.storage.setItem('dark', false);
      }else{
        dark.storage.setItem('dark', true);
      }
  },
  init: function(){
    if(dark.storage.getItem('dark') === "true"){
      dark.toggle();
      dark.storage.setItem('dark', true);
    }else{
      dark.storage.setItem('dark', false);
    }
  },
  storage: null
};
dark.storage = window.localStorage;
dark.init();
